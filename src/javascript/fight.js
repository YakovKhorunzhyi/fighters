export function fight(firstFighter, secondFighter) {
    let healthFirst = firstFighter.health;
    let healthSecond = secondFighter.health;

    do {
        healthFirst -= getDamage(secondFighter, firstFighter);
        healthSecond -= getDamage(firstFighter, secondFighter);
    } while (healthFirst >= 0 && healthSecond >= 0);

    return healthFirst > 0 ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
    return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
    return fighter.attack * criticalHitChance();
}

export function criticalHitChance() {
    return Math.random() + 1;
}

export function getBlockPower(fighter) {
    return fighter.defense * dodgeChance();
}

export function dodgeChance() {
    return Math.random() + 1;
}
